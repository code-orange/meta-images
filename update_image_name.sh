#!/bin/bash
set -e
set -x

for image_name in */; do
    value_image_name=`echo $image_name | sed 's/\///g'`
    cd $value_image_name
    git pull || true
    sed -i -E "s/hostname: (.*)/hostname: $value_image_name/g" imageconf/vmtemplate.yml
    sed -i -E "s/^name: (.*)/name: $value_image_name/g" imageconf/vmtemplate.yml
    git add imageconf/vmtemplate.yml
    git rm -f README*
    echo "# $value_image_name" > README.md
    git add README.md
    printf $value_image_name > copy-to-filesystem/etc/cmdb/node_type
    git add copy-to-filesystem/etc/cmdb/node_type
    git commit -m "normalize image name" || true
    git push || true
    cd ../
done
