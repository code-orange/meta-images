#!/bin/bash

urls=`git submodule foreach git config --get remote.origin.url | grep -E '(http|https)://'`

true > ./Jenkinsfile

echo 'node {' >> ./Jenkinsfile

while read -r line; do
    if [[ $line =~ "centos" || $line =~ "netstack-router-legacy" ]]
    then
       echo "Backlist entry found: $line"
       continue
    fi

    IMAGE_NAME=`echo $line | awk -F[/:] '{print $6}'`

    echo "    stage('${IMAGE_NAME}') {" >> ./Jenkinsfile
    echo "        build propagate: false, job: 'vm_image_build', parameters: [string(name: 'VM_GIT_URL', value: '$line')]" >> ./Jenkinsfile
    echo "    }"  >> ./Jenkinsfile
done <<< "$urls"

echo '}' >> ./Jenkinsfile

git add Jenkinsfile

git commit -a -m "update Jenkinsfile"
git push origin master
