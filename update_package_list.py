import json
import os

import yaml


def list_remove_duplicates(x):
    return list(dict.fromkeys(x))


packages_file = open("package_list.json", "w+")
sources_file = open("sources_list.json", "w+")

apps = list()
sources = list()

for fs_walk in os.walk("."):
    for image in fs_walk[1]:
        print("Image: " + image)

        try:
            with open(image + "/imageconf/vmtemplate.yml", "rt") as stream:
                file_content = yaml.safe_load(stream)

                apps += file_content["bootstrapper"]["include_packages"]
                apps += file_content["packages"]["install"]

                for source_name, source in file_content["packages"]["sources"].items():
                    if source is not None:
                        sources += source

        except FileNotFoundError:
            continue

    break

apps = sorted(list_remove_duplicates(apps))
sources = sorted(list_remove_duplicates(sources))

json.dump(apps, packages_file, sort_keys=True, indent=4)
json.dump(sources, sources_file, sort_keys=True, indent=4)
